﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <sstream>
#include <ctime>

void Print_num(int N, int k)
{
    for (int i = k; i <= N; i += 2)
       std::cout << i << " ";
    std::cout << "\n";
}

int** Create(size_t n, size_t m)
{
    int** M = new int* [n];
    for (size_t i = 0; i < n; ++i)
    {
        M[i] = new int[m];
    }
    return M;
}

void Free(int** M, size_t n)
{
    for (size_t i = 0; i < n; ++i)
    {
        delete[] M[i];
    }
    delete[] M;
}

void Input(int** M, size_t n, size_t m)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            M[i][j] = i + j;
        }
    }
}

void Print(int** M, size_t n, size_t m)
{
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < m; ++j)
        {
           std::cout << M[i][j] << ' ';
    }
      std::cout << std::endl;
  }
}

int main()
{
  setlocale(LC_ALL, "rus");

  int sum = 0;
  int m = 0;
  size_t n;
  int day = 12;
    
  std::cout << "Введите значение N: ";
  std::cin >> n;

  std::cout << "Введите размерность матрицы: ";
  std::cin >> n;
  int** A = Create(n, n);
  Input(A, n, n);
  Print(A, n, n);

  //Free(A, n);

  int target_row_index = day % n;

  for (int j = 0; j < n; j++)

  {

      sum = sum + A[target_row_index][j];
      
  }

    std::cout << sum;

    Free(A, n);

  //if (sum != NULL)
  //{
      //delete[] sum;
      //sum = NULL;
  //}

  return 0;
}
